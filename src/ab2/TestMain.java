package ab2;

import ab2.impl.DippoldMittererSharifi.HashMapLinear;
import ab2.impl.DippoldMittererSharifi.HashMapQuadratic;

public class TestMain {
    public static void main(String[] args) {
        HashMapQuadratic qhm = new HashMapQuadratic(10);

        System.out.println(qhm.totalSize());

        qhm.put(12, "first");
        qhm.put(12, "second");
        qhm.put(12, "third");
        qhm.put(22, "lol");
        qhm.put(32, "lol");
        qhm.put(42, "lol");
        qhm.put(22, "lol");

        System.out.println(qhm.get(12));
        System.out.println(qhm.elementCount());
    }
}
