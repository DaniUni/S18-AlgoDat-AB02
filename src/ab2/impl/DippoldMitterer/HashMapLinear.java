package ab2.impl.DippoldMittererSharifi;

import ab2.AbstractHashMap;

public class HashMapLinear extends AbstractHashMap {
    private int currentElementCount;

    public HashMapLinear(int minSize) {
        this.initTable(minSize);
        this.currentElementCount = 0;
    }

    /**
     * Fügt ein Key-Value-Paar in die Hashtabelle ein. Wird ein Schlüssel abermals
     * eigefügt, soll das gespeicherte Value-Objekt ersetzt werden.
     *
     * @param key   Schlüssel, darf nicht null sein
     * @param value
     * @return liefert false, falls die Tabelle bereits voll ist
     */
    @Override
    public boolean put(int key, String value) {
        if (this.currentElementCount >= this.totalSize())
            return false;

        int newIdx = key % this.totalSize();

        while (!this.isEmpty(newIdx) && this.getKey(newIdx) != key) {
//            System.out.printf("position %d not empty...%n", newIdx);
            newIdx--;
        }

        if (!this.isEmpty(newIdx) && this.getKey(newIdx) == key) {
//            System.out.println("overwriting a already inserted key");
        } else {
            this.currentElementCount++;
        }
        this.setKeyAndValue(newIdx, key, value);
//        System.out.printf("Added value = %s with key = %d at index %d%n", value, key, newIdx);
        return true;
    }

    /**
     * Liefert das gespeicherte Value-Objekt oder null, falls unter dem Schlüssel
     * nichts gespeichert ist
     *
     * @param key
     * @return null, falls der Schlüssel nicht vorkomment, andernfalls das
     * dazugehörige Objekt
     */
    @Override
    public String get(int key) {
        int newIdx = key % this.totalSize();

        for (int i = 0; i < this.totalSize(); i++) {
            if (this.isEmpty(newIdx))
                return null;
            if (this.getKey(newIdx) == key) {
                return this.getValue(newIdx);
            }
            newIdx--;
            if (newIdx < 0) newIdx += this.totalSize() - 1;
        }

        return null;
    }

    /**
     * Liefert die Anzahl der gespeicherten Elemente in der Hashtabelle
     *
     * @return Anzahl der gespeicherten Elemente
     */
    @Override
    public int elementCount() {
        return this.currentElementCount;
    }
}
