package ab2.impl.DippoldMittererSharifi;

import ab2.AbstractHashMap;
import ab2.AuD2;

public class AuD2Impl implements AuD2 {

	@Override
	public AbstractHashMap newHashMapLinear(int minSize) {
		return new HashMapLinear(minSize);
	}

	@Override
	public AbstractHashMap newHashMapQuadratic(int minSize) {
		return  new HashMapQuadratic(minSize);
	}

	@Override
	public AbstractHashMap newHashMapDouble(int minSize) {
		return null;
	}

	@Override
	public int getMedian(int[] data) {
		return Medians.MediansofMedians(data, data.length / 2 + 1);
	}
}
