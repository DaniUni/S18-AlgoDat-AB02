package ab2.impl.DippoldMittererSharifi;


public class Medians {

    public static int MediansofMedians(int[] vals, int i) {
        if (vals.length <= 10) {
            insertionSort(vals);
            return vals[i];
        }

        int[][] groups = getGroups(vals);
        int[] medians = new int[groups.length];

        for (int j = 0; j < groups.length; j++) {
            medians[j] = MediansofMedians(groups[j], groups[j].length / 2);
        }

        int mom = MediansofMedians(medians, medians.length / 2);

        if (doesPositionFit(vals, mom, i)) {
            return mom;
        }

        int pivotPosition = pivot(vals, mom);

        if (i == pivotPosition)
            return mom;

        if (i <= pivotPosition) {
//            System.out.println("YEY: " + Arrays.toString(createArray(vals, 0, pivotPosition)));
            return MediansofMedians(createArray(vals, 0, pivotPosition), i);
        } else {
//            System.out.println("YOY: " + Arrays.toString(createArray(vals, pivotPosition + 1, vals.length )));
            return MediansofMedians(createArray(vals, pivotPosition + 1, vals.length ), i - pivotPosition - 1);
        }
    }

    public static void insertionSort(int[] data){
        for (int i = 1; i < data.length; i++) {
            boolean inserted = false;
            int j = i;

            while (j >= 1 && !inserted) {
                if (data[j] < data[j - 1]) {
                    swap(data, j, j - 1);
                } else {
                    inserted = true;
                }
                j--;
            }
        }
    }

    private static void swap(int[] vals, int pos1, int pos2) {
        int temp = vals[pos1];
        vals[pos1] = vals[pos2];
        vals[pos2] = temp;
    }

    public static int[][] getGroups(int[] vals) {
        int numGroups = (int)Math.ceil(vals.length / 5f);
        int counter = 0;

        int[][] grps = new int[numGroups][];

        for (int i = 0; i < numGroups; i++) {
            int currentElementsLeft = vals.length - counter;
            int sizeToUse = currentElementsLeft >= 5 ? 5 : currentElementsLeft;
            grps[i] = new int[sizeToUse];

            for (int j = 0; j < sizeToUse; j++) {
                grps[i][j] = vals[counter];
                counter++;
            }
        }

        return grps;
    }

    /* For debugging */
//    private static void debugGroups(int[][] grps) {
//        System.out.println("length: " + grps.length);
//        Arrays.stream(grps).forEach(g -> {System.out.println("<" + g.length + "> " + Arrays.toString(g)); });
//    }

    private static int[] createArray(int[] vals, int pos1, int pos2) {
        int len = pos2 - pos1;
        int[] result = new int[len];

        for (int i = 0; i < len; i++) {
            result[i] = vals[pos1 + i];
        }

        return result;
    }

    private static int pivot(int[] vals, int v) {
        int lo = 0;
        int hi = vals.length;

        while (lo < hi) {
            if (v > vals[lo]) {
                lo++;
            } else if (v < vals[lo]) {
                swap(vals, lo, --hi);
            } else {
                while (hi > lo + 1) {
                    int e = vals[--hi];
                    if (v >= e) {
                        if (lo + 1 == hi) {
                            swap(vals, lo, lo + 1);
                            lo++;
                            break;
                        }
                        swap(vals, lo, lo + 1);
                        swap(vals, lo, hi);
                        lo++;
                        hi++;
                    }
                }
                break;
            }
        }

        return lo;
    }

    private static boolean doesPositionFit(int[] vals, int mom, int i) {
        for (int j = 0; j < vals.length; j++) {
            if (vals[j] == mom && j == i)
                return true;
        }
        return false;
    }
}
