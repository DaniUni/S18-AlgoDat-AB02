//package ab2.impl.DippoldMittererSharifi;
//
//public class MediansOfMedians {
//    public static int select(int[] list, int lo, int hi, int k) {
//        if (lo >= hi || k < 0 || lo + k >= hi)
//            throw new IllegalArgumentException();
//        if (hi - lo < 10) {
//            Collections.sort(list.subList(lo, hi));
//            createArray();
//            return lo + k;
//        }
//        int s = hi - lo;
//        int np = s / 5; // Number of partitions
//        for (int i = 0; i < np; i++) {
//            // For each partition, move its median to front of our sublist
//            int lo2 = lo + i * 5;
//            int hi2 = (i + 1 == np) ? hi : (lo2 + 5);
//            int pos = select(list, lo2, hi2, 2);
//            Collections.swap(list, pos, lo + i);
//        }
//
//        // Partition medians were moved to front, so we can recurse without making another list.
//        int pos = select(list, lo, lo + np, np / 2);
//
//        // Re-partition list to [<pivot][pivot][>pivot]
//        int m = triage(list, lo, hi, pos);
//        int cmp = lo + k - m;
//        if (cmp > 0)
//            return select(list, m + 1, hi, k - (m - lo) - 1);
//        else if (cmp < 0)
//            return select(list, lo, m, k);
//        return lo + k;
//    }
//
//    public static void insertionSort(int[] data){
//        for (int i = 1; i < data.length; i++) {
//            boolean inserted = false;
//            int j = i;
//
//            while (j >= 1 && !inserted) {
//                if (data[j] < data[j - 1]) {
//                    swap(data, j, j - 1);
//                } else {
//                    inserted = true;
//                }
//                j--;
//            }
//        }
//    }
//
//    private static void swap(int[] vals, int pos1, int pos2) {
//        int temp = vals[pos1];
//        vals[pos1] = vals[pos2];
//        vals[pos2] = temp;
//    }
//
//    private static int[] createArray(int[] vals, int pos1, int pos2) {
//        int len = pos2 - pos1;
//        int[] result = new int[len];
//
//        for (int i = 0; i < len; i++) {
//            result[i] = vals[pos1 + i];
//        }
//
//        return result;
//    }
//
//}
